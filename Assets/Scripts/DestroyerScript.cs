﻿using UnityEngine;
using System.Collections;

public class DestroyerScript : MonoBehaviour 
{

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag == "Player")
		{
			//Debug.Log ("Player object collision");
			Application.LoadLevel (1);
		}

		if(other.gameObject.transform.parent)
		{
			//Debug.Log ("Game object collision");
			Destroy (other.gameObject.transform.parent.gameObject);
		}
        
	//Causing the game to crash
	//else
        //{
        //    Destroy (other.gameObject);
        //}
	}
}
