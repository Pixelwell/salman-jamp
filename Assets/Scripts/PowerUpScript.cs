﻿using UnityEngine;
using System.Collections;

public class PowerUpScript : MonoBehaviour {

	HUDScript hud;

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag == "Player")
		{

			Debug.Log("Power Up Collision");
			hud = GameObject.Find ("Main Camera").GetComponent<HUDScript>();
			hud.increaseScore(10);
			Destroy (this.gameObject);
		}
	}
}
